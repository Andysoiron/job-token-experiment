# Job token experiment

This is an experiment to see if source code can be fetched by a pipeline.

## Result

Fetching a file contenct get's rejected with `401`.

![Job log screenshot showing 401 error](job-log.png)
